import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import * as xml2js from 'xml2js';

import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
    baseUrl = 'http://api.notified.com/Channel/4b100c3e-ec80-e711-80d8-a0d3c1f2861f?key=ab07bd54-7766-46dd-8480-2daa14035af0';

    constructor(private _http: Http) {}

    fetchFeeds() {
        const headers = new Headers({
            'Accept': 'application/xml'
        });
        const options = new RequestOptions({headers: headers});

        return this._http.get(this.baseUrl, options);
    }
}
