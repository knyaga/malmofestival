import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'htmlToTextRender'
})
export class HtmlToTextRenderPipe implements PipeTransform {

  transform(value: any): string {
    return  value ? String(value).replace(/<[^>]+>/gm, '') : '';
  }

}
