import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {Ng2PaginationModule} from 'ng2-pagination';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PostsComponent } from './components/posts/posts.component';

import { ApiService } from './services/api.service';
import { HtmlToTextRenderPipe } from './pipes/html-render.pipe';
import { AboutComponent } from './components/about/about.component';
import { PartnersComponent } from './components/partners/partners.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PostsComponent,
    HtmlToTextRenderPipe,
    AboutComponent,
    PartnersComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    Ng2PaginationModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
