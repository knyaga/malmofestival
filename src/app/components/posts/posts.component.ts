import { Component, OnInit } from '@angular/core';
import * as xml2js from 'xml2js';

import { ApiService } from './../../services/api.service';

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
    posts: any = [];
    page = 1;

    constructor(private apiService: ApiService) {}

    ngOnInit() {
        this.apiService.fetchFeeds().subscribe( response => {
            xml2js.parseString(response.text(), (err, result) => {
              const data = result.rss.channel[0].item;
              this.posts = data;
            });
            console.log(this.posts);
        });
    }




}
