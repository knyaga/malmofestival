import { MalmofestivalPage } from './app.po';

describe('malmofestival App', () => {
  let page: MalmofestivalPage;

  beforeEach(() => {
    page = new MalmofestivalPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
