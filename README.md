# Malmofestival

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.7.

## Dependencies - Development phase
This project requires angular cli 1.2.7+ and need to disable browser security policies for CORS due to the RSS feed api does not allow CORS. However the live version  of the site can be found `http://malmofestivalen.s3-website.eu-central-1.amazonaws.com/`

## Development server
1. First `npm install` to install the node modules.
2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.